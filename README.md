# Computer Graphics Project

A 3D world created using OpenGL.

## Features

- Procedural terrain generation
- Procedural tree generation
- Infinite terrain
- Water
- Water ripples
- Underwater view

## Group chat

[![Gitter](https://badges.gitter.im/nunomota/Computer-Graphics.svg)](https://gitter.im/nunomota/Computer-Graphics?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=body_badge)